package bot;

import poker.HandHoldem;
import poker.PokerMove;
import poker.Card;

import java.util.Random;
import bot.BotState;

import com.stevebrecher.HandEval;
/**
 * Created by algentile on 5/2/16.
 */
public class myholdem implements Bot {

    /*
    Create an array to keep track of the raises that the opponent makes.
    If the opponent makes higher check calls or more aggressive, we hope to capitalize
    and punish for over aggressiveness.
     */
    private int[] raises = new int[100];
    private double raise_factor = .82;
    private int all;

    //Learning rates
    private double gamma = .1; //proness to aggressive play
    private double alpha = .3; //learning rate
    private double epsilon = .5; //proness to fold in order to throw off opponent
    /*
    Check the frequency of the opponents raises over the specified number of rounds. If we catch them
    being over aggressive, mainly with an aggressive rate of 60% or higher, the bit will punish the opposing
    player by upping its raise and all in calls to respond to a possible bluff. We also make sure that we maintain
    a sample size of at least 5 rounds before we take action.
     */
    public boolean eval_opponent_moxy(BotState state,int[] raise_array){
        PokerMove opponents_State = state.getOpponentAction();
        int moxy = 0;
        int hesitant = 0;
        boolean is_aggressive = false;
        for(int i = 0; i < raise_array.length; i++){
            if(raise_array[i] >= 2 && raise_array[i] != 0){
                moxy++;
            }
            if(raise_array[i] < 2 && raise_array[i] !=  0){
                hesitant = 0;
            }

            else{
                if((double)(moxy/i-1) >= .60 && i > 5){
                    is_aggressive = true;
                }
                else is_aggressive = false;
            }
        }
        return is_aggressive;
    }

    //The bot will fold a few hands in order to mess with the opponents reads
    public PokerMove evaluate_bluffoptions(HandHoldem hand, Botstate state,weight1, weight2){
        if(state.getmyStack() > 2500 && ((weight1 < 5) || (weight2 < 5))){
            epsilon = epsilon * .9
            return new PokerMove(state.getMyName(), 'check', 0);
        }
        if(epsilon < .3){
            epsilon = epsilon + .1;
            return new PokerMove(state.getMyName(), 'raise',2*state.getBigBlind())
        }
        else return new PokerMove(state.getMyName(), 'check', 0);
    }

    /*
    Check the ordinal values of our hand with the board, if the probability that the move
   */
    @Override
    public PokerMove getMove(BotState state, Long timeOut) {
        Random random = new Random();
        HandHoldem hand = state.getHand();
        String handCategory = getHandCategory(hand, state.getTable()).toString();

        //Get the weight strengths of bot's hand preflop
        int card1_weight = hand.getCard(0).getHeight().ordinal();
        int card2_weight = hand.getCard(1).getHeight().ordinal();

        PokerMove opponentsMove = state.getOpponentAction();
        int curr_round = state.getRound();
        if(card1_weight == card2_weight){
            return new PokerMove(state.getMyName(), "raise", state.getBigBlind() + 100);
        }
        if(eval_opponent_moxy(state,raises) && all < 4){
            all++;
            double new_bet = raise_factor * all;
            return new PokerMove(state.getMyName(), "raise", (int)(100 * new_bet));
        }
        if(eval_opponent_moxy(state,raises) && all > 4){
            all = 0;
            return new PokerMove(state.getMyName(), "raise", state.getmyStack());
        }

         if(card1_weight > 5 && card2_weight > 5){
            return new PokerMove(state.getMyName(),"call", state.getmyStack());
        }
            if(opponentsMove.getAction() == "raise") {
                raises[curr_round] += 1;
                double vary_moves = random.nextDouble();
                if(vary_moves < .70){
                    return new PokerMove(state.getMyName(),"call", state.getAmountToCall());
                }
                else return new PokerMove(state.getMyName(), "raise", 100);
            }
        return new PokerMove(state.getMyName(),"check",0);
    }

    public HandEval.HandCategory getHandCategory(HandHoldem hand, Card[] table) {
        if( table == null || table.length == 0 ) { // there are no cards on the table
            return hand.getCard(0).getHeight() == hand.getCard(1).getHeight() // return a pair if our hand cards are the same
                    ? HandEval.HandCategory.PAIR
                    : HandEval.HandCategory.NO_PAIR;
        }
        long handCode = hand.getCard(0).getNumber() + hand.getCard(1).getNumber();

        for( Card card : table ) { handCode += card.getNumber(); }

        if( table.length == 3 ) { // three cards on the table
            return rankToCategory(HandEval.hand5Eval(handCode));
        }
        if( table.length == 4 ) { // four cards on the table
            return rankToCategory(HandEval.hand6Eval(handCode));
        }
        return rankToCategory(HandEval.hand7Eval(handCode)); // five cards on the table
    }

    public HandEval.HandCategory rankToCategory(int rank) {
        return HandEval.HandCategory.values()[rank >> HandEval.VALUE_SHIFT];
    }


    public static void main(String[] args){
        BotParser parser = new BotParser(new myholdem());
        parser.run();
    }

} //end of main class
